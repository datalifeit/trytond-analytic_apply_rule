# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import sale
from . import purchase
from . import invoice


def register():
    Pool.register(
        sale.Rule,
        sale.MoveLine,
        sale.Sale,
        sale.SaleLine,
        module='analytic_apply_rule', type_='model',
        depends=['analytic_sale'])
    Pool.register(
        purchase.Rule,
        purchase.MoveLine,
        purchase.Purchase,
        purchase.PurchaseLine,
        module='analytic_apply_rule', type_='model',
        depends=['analytic_purchase'])
    Pool.register(
        invoice.Rule,
        invoice.MoveLine,
        invoice.Invoice,
        invoice.InvoiceLine,
        module='analytic_apply_rule', type_='model',
        depends=['analytic_invoice'])
