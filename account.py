# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import Pool


class ApplyRuleMixin(object):

    @fields.depends('analytic_accounts',
            methods=['_get_analytic_rule_template'])
    def apply_analytic_rule(self):
        pool = Pool()
        Rule = pool.get('analytic_account.rule')

        template = self._get_analytic_rule_template()
        if not template:
            return

        rules = Rule.search([])

        pattern = self.rule_analytic_pattern(template)
        for skip_field in (None, 'party'):
            _pattern = pattern.copy()
            if skip_field is not None:
                _pattern[skip_field] = None
            for rule in rules:
                # todo: find rule with and without party
                if rule.match(_pattern):
                    break
            else:
                continue
            break
        else:
            return
        analytic_accounts = list(self.analytic_accounts)
        for entry in rule.analytic_accounts:
            analytic_account = [a for a in analytic_accounts
                if a.root == entry.root]
            if not analytic_account:
                continue
            analytic_account, = analytic_account
            analytic_account.account = entry.account
        self.analytic_accounts = analytic_accounts

    def rule_analytic_pattern(self, template):
        return {
            'company': template.company.id if template.company else None,
            'account': None,
            'journal': None,
            'party': template.party.id if template.party else None,
        }

    @classmethod
    def copy(cls, records, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default.setdefault('analytic_accounts')
        new_records = super().copy(records, default=default)

        if not default.get('analytic_accounts'):
            accounts = cls.default_analytic_accounts()
            if accounts:
                cls.write(new_records, {
                    'analytic_accounts': [('create', accounts)]})
                for new_record in new_records:
                    new_record.apply_analytic_rule()
                    new_record.save()
        return new_records
